export const state = () => ({
  settings: [],
})
export const mutations = {
  SET_SETTINGS(state, payload) {
    state.settings = payload
  },
}
export const actions = {
  setSettings({ commit }) {
    this.$axios('/settings').then((res) => {
      const { data } = res.data
      // const convertedSettings = settings.reduce((acc, currentSetting) => {
      //   acc[currentSetting.type] = currentSetting.value
      //   return acc
      // }, {})

      commit('SET_SETTINGS', data)
      return data
    })
  },
}
export const getters = {
  getSettings: (state) => {
    return state.settings
  },
}
