export const state = () => ({
  dialog: false,
})
export const mutations = {
  SET_DIALOG(state) {
    state.dialog = !state.dialog
  },
}
export const actions = {
  setDialog({ commit }) {
    commit('SET_DIALOG')
  },
}
export const getters = {
  getDialog(state) {
    return state.dialog
  },
}
