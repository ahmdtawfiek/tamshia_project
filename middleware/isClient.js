export default ({ $auth, redirect, app }) => {
  if ($auth.loggedIn && $auth.user.type !== 'client') {
    redirect(app.localePath('/'))
  }
}
