export default ({ $auth, redirect, app }) => {
  if (!$auth.user.type === 'COMPANY') {
    redirect(`/${app.i18n.locale}`)
  }
}
