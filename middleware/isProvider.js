export default ({ $auth, redirect, app }) => {
  if ($auth.loggedIn && $auth.user.type == 'guide') {
    redirect(app.localePath('/'))
  }
}
