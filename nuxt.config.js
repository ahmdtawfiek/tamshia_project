export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s',
    title: 'تمشية',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      { hid: 'description', name: 'description', content: '' },
    ],
    // link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/scss/main.scss'],
  pageTransition: {
    name: 'fade',
    mode: 'out-in',
  },
  loadingIndicator: {
    name: 'cube-grid',
    color: '#f00',
    background: 'white',
  },
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,
  // components: {
  //     dirs: [
  //         '~/components',
  //         {
  //             path: '~/components/base/',
  //             prefix: 'Base',
  //         },
  //     ],
  // },
  // target: 'static',
  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
    // https://google-fonts.nuxtjs.org
    '@nuxtjs/google-fonts',
  ],
  googleFonts: {
    families: {
      Cairo: true,
    },
  },
  moment: {
    defaultLocale: 'en',
    locales: ['ar'],
  },
  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '~/modules/example',
    'nuxt-i18n',
    'vue-social-sharing/nuxt',
    [
      'nuxt-sweetalert2',
      {
        showCancelButton: true,
        focusConfirm: false,
      },
    ],
    'nuxt-gmaps',
    //https://image.nuxtjs.org
    // '@nuxt/image'
  ],
  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en.json',
        name: 'english',
      },
      {
        code: 'ar',
        file: 'ar.json',
        name: 'arabic',
      },
    ],
    defaultLocale: 'ar',
    strategy: 'prefix_and_default',
    langDir: 'locales/',
    lazy: true,
    useCookie: true,
    cookieKey: 'language',
    detectBrowserLanguage: false,
    vueI18n: {
      fallbackLocale: 'ar',
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['@/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: false,
      // themes: {
      //     dark: {
      //         primary: colors.blue.darken2,
      //         accent: colors.grey.darken3,
      //         secondary: colors.amber.darken3,
      //         info: colors.teal.lighten1,
      //         warning: colors.amber.base,
      //         error: colors.deepOrange.accent4,
      //         success: colors.green.accent3,
      //     },
      // },
      themes: {
        light: {
          primary: '#029794',
          'light-primary': '#e7ffe7',
          'light-white': '#F7F7F7',
          secondary: '#7960DA',
          'light-secondary': '#ded2f4',
          gray: '#383838',
          'light-gray': '#888888',
          warning: '#FFB229',
          success: '#029F02',
          'off-white': '#FBFBFB',
          // accent: colors.grey.darken3,
          // secondary: colors.amber.darken3,
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          // error: colors.deepOrange.accent4,
          // success: colors.green.accent3,
        },
      },
    },
  },
  serverMiddleware: ['~/middleware/logger'],
  server: {
    // port: 8000, // default: 3000
    // host: '0.0.0.0', // default: localhost,
    port: '3080',
    timing: false,
  },
  router: {
    middleware: ['routeChange'],
    // middleware: ['isAuth'],
    // extendRoutes(routes, resolve) {
    //     const newRoutes = [...routes, ...customRoutes]
    //     newRoutes.map(route => {
    //         routes.push({
    //             ...route,
    //             component: resolve(__dirname, route.component),
    //         })
    //     })
    // },
  },
  axios: {
    baseURL: 'https://sanedapps.com/tamshia/public/api',
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'token',
        },

        endpoints: {
          login: {
            url: '/login',
            method: 'post',
            propertyName: 'data.token',
          },
          logout: { url: '/userLogout', method: 'post' },
          user: false,
          // user: {
          //   url: '/myProfile',
          //   method: 'get',
          //   // propertyName: 'data',
          //   // headers: { 'accept-language': 'ar' },
          // },
        },
        redirect: false,
      },
      // google: {
      //   _scheme: 'oauth2',
      //   authorization_endpoint: 'https://accounts.google.com/o/oauth2/auth',
      //   userinfo_endpoint: 'https://www.googleapis.com/oauth2/v3/userinfo',
      //   scope: ['openid', 'profile', 'email'],
      //   response_type: 'token',
      //   token_type: 'Bearer',
      //   redirect_uri: 'http://localhost:3000',
      //   token_key: 'access_token',
      //   state: 'UNIQUE_AND_NON_GUESSABLE',
      //   client_id:
      //     '626024501228-l454fo04cqsoo96289f9ts1kclkee2i1.apps.googleusercontent.com',
      // },
      // facebook: {
      //   endpoints: {
      //     userInfo:
      //       'https://graph.facebook.com/v6.0/me?fields=id,name,picture{url}',
      //   },
      //   clientId: '793531764902698',
      //   scope: ['public_profile', 'email'],
      // },
    },
    plugins: [{ src: '~/plugins/axios', ssr: true }],
  },
  plugins: [
    { src: '@/plugins/api.js' },
    { src: '@/plugins/vuetify.js' },
    { src: '@/plugins/initializer.js', ssr: false },
    { src: '@/plugins/facebook.js', ssr: false },
    { src: '@/plugins/vuelidate.js' },
    { src: '@/plugins/toasted.js', ssr: false },
  ],
  loadingIndicator: {
    name: 'cube-grid',
    color: '#f00',
    background: 'white',
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    // You can extend webpack config here
    // config.resolve.alias['~'] = './store_submodules'
    // Run ESLint on save
    extractCSS: true,
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        })
      }
    },
  },
}
