import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import CowIcon from '@/assets/icons/cow-icon'
import ArrowDown from '@/assets/icons/arrow-down'
import ResendIcon from '@/assets/icons/resend'
Vue.use(Vuetify)

export default new Vuetify({
  rtl: true,
  icons: {
    iconfont: 'mdi',
    values: {
      cowIcon: {
        component: CowIcon,
      },
      arrowDown: {
        component: ArrowDown,
      },
      resendIcon: {
        component: ResendIcon,
      },
    },
  },
})
