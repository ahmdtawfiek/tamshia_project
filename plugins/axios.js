import Vue from 'vue'
export default ({ store, app: { $axios, i18n, $auth } }) => {
  $axios.onResponse((response) => {
    if (store.getters['errors/serverErrors'] !== null) {
      store.dispatch('errors/ClearServerErrors')
    }
    const { method, url } = response.config

    if (
      (url !== '/suggestedServices' &&
        url !== '/discoverItems' &&
        url !== '/service_map' &&
        method === 'post') ||
      method === 'put' ||
      method === 'delete'
    ) {
      Vue.toasted.success(
        response.data.success_msg || i18n.t('success_message')
      )
    }

    return response
  })
  $axios.onRequest((config) => {
    config.headers.common['X-locale'] = i18n.locale
    config.headers.common['Accept-Language'] = i18n.locale
    config.headers.common.Timezone = `UTC +${
      -new Date().getTimezoneOffset() / 60
    }`

    return config
  })

  $axios.onError((err) => {
    if (err.response && err.response.status === 403) {
      Vue.toasted.error(err.response.data.message)
      // $auth.logout()
    }
    if (err.response && err.response.status === 401) {
      Vue.toasted.error(err.response.data.message)
      // $auth.logout()
    }

    if (err.response && err.response.data && err.response.data.error) {
      store.dispatch('errors/SetServerErrors', err.response.data.error)
      Vue.toasted.error(err.response.data.message)
    }
  })
}
