import Vue from 'vue'
import { mapGetters } from 'vuex'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
import * as VueGoogleMaps from 'vue2-google-maps'
import ReadMore from 'vue-read-more'

import {
  Swiper as SwiperClass,
  Pagination,
  Mousewheel,
  Autoplay,
} from 'swiper/js/swiper.esm'

SwiperClass.use([Pagination, Mousewheel, Autoplay])
Vue.use(VueAwesomeSwiper)
Vue.use(ReadMore)
Vue.mixin({
  computed: {
    ...mapGetters({
      serverErrors: 'errors/serverErrors',
    }),
  },
  methods: {
    popUp() {
      return new Promise((resolve, reject) => {
        this.$swal
          .fire({
            title: this.$t('message.alert_message'),
            text: this.$t('message.sub_text'),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.$t('button.delete_alert'),
            cancelButtonText: this.$t('button.cancel_alert'),
          })
          .then((result) => {
            if (!result.dismiss) {
              return resolve(true)
            }
          })
          .catch(() => reject(false))
      })
    },
  },
})
Vue.prototype.$eventBus = new Vue()

Vue.use(VueGoogleMaps, {
  load: {
    // key: 'YOUR_API_TOKEN',
    libraries: 'places',
  },
})
